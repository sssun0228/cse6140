import networkx as nx
import heapq
from networkx.utils import UnionFind
import time
import sys

class approximation:
    def read_graph(self, filename):
       
        G = nx.MultiGraph()
        with open(filename, 'r') as file:
            first_line = file.readline()
            (n, m, k) = first_line.split()
            
            for u in range(0, int(n)):
                line = file.readline()
                edge_data = list(map(lambda x: int(x), line.split()))
                
                G.add_node(u)
                
                for v in edge_data:
                    G.add_edge(u, int(v)-1)
#             firstLine = False
#             i = 0
#             
#             for line in file:
#                 if (firstLine == False):
#                     firstLine = True
#                     continue
#                 
#                 nodes = line.split()
#                 
#                 for node in nodes:
#                      G.add_edge(i, int(node)-1)
# 
#                 i += 1

        return G
    
    def approx_vertex_cover(self, graph):
        C = set()
        
        allEdges = set() #[]
        for edge in graph.edges():
            if not edge in allEdges:
                allEdges.add(edge) #heapq.heappush(allEdges, edge)
        
#         heapq.heapify(allEdges)
        while allEdges:
            temp_edge = allEdges.pop() #heapq.heappop(allEdges)
            u = temp_edge[0]
            v = temp_edge[1]
            C.add(u)
            C.add(v)
#             C.union(u, v)
            if (len(graph.adjacency_list()[u]) != 0):
                for u_neighbor in graph.adjacency_list()[u]:
                    if (u, u_neighbor) in allEdges:
                        allEdges.remove((u, u_neighbor))
                    if (u_neighbor, u) in allEdges:
                        allEdges.remove((u_neighbor, u))
            
            if (len(graph.adjacency_list()[v]) != 0):
                for v_neighbor in graph.adjacency_list()[v]:
                    if (v, v_neighbor) in allEdges:
                        allEdges.remove((v, v_neighbor))
                    if (v_neighbor, v) in allEdges:
                        allEdges.remove((v_neighbor, v))

        return C
                
    def main(self):

#         num_args = len(sys.argv)
# 
#         if num_args < 4:
#             print "error: not enough input arguments"
#             exit(1)
        
        graph_file = "C:\\Users\\schoi332\\Documents\\COURSE\\cse6140\\project\\Data\\star.graph" #sys.argv[1] #
        output_file = "C:\\Users\\schoi332\\Documents\\COURSE\\cse6140\\project\\Solutions\\star22.txt" #sys.argv[3] #

        #Construct graph
        G = self.read_graph(graph_file)

        start_approx = time.clock() #time in seconds
        VC = self.approx_vertex_cover(G) 
        total_time = (time.clock() - start_approx) * 1000 #to convert to milliseconds

        #Write initial MST weight and time to file
        output = open(output_file, 'w')
        output.write(str(len(VC)) + " " + str(total_time) + "\n")
        
        for i in range(0, len(VC)):
            if i != 0:
                output.write(", ")
            output.write(str(list(VC)[i]+1))

       
        
if __name__ == '__main__':
    # run the experiments
    runexp = approximation()
    runexp.main()

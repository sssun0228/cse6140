import networkx as nx
from operator import itemgetter 
from networkx.utils import UnionFind
import time
import sys
import math

class approximation:
    def read_graph(self, filename):
       
        G = nx.Graph()
        with open(filename, 'r') as file:
            first_line = file.readline()
            (n, m, k) = first_line.split()
            
            for u in range(0, int(n)):
                line = file.readline()
                edge_data = list(map(lambda x: int(x), line.split()))
                
                G.add_node(u)
                
                for v in edge_data:
                    G.add_edge(u, int(v)-1)

        return G
    

    def approx_vertex_cover_priority(self, graph):
        C = set()
#         updatedG = nx.Graph()
        updatedG = graph
        allEdges = set() #[]                         
#         verticeDegree = list(graph.degree_iter())
#         verticeList = sorted(verticeDegree, key=itemgetter(1), reverse=True)
        
        while(len(updatedG.edges()) != 0): #i in range(0, len(updatedG.nodes())):#verticeList)):

#             for edge in updatedG.edges():#graph.edges():
#                 if not edge in allEdges:
#                     allEdges.add(edge)   

            verticeDegree = list(updatedG.degree_iter()) #graph.degree_iter())
            verticeFiltered = [x for x in verticeDegree if x[1] != 0]
            verticeList = sorted(verticeFiltered, key=itemgetter(1), reverse=True)

            for i in range(0, len(verticeList)):#int(math.floor(len(graph.nodes())/5))):
                tempVertice = verticeList[i][0]
                if (len(updatedG.adjacency_list()[tempVertice]) != 0):
                    C.add(tempVertice)
                    for u_neighbor in updatedG.adjacency_list()[tempVertice]:#graph.adjacency_list()[tempVertice]:
#                         updatedG.remove_edge(tempVertice, u_neighbor)
                        if (tempVertice, u_neighbor) in updatedG.edges():
                            updatedG.remove_edge(tempVertice, u_neighbor)
                        if (u_neighbor, tempVertice) in updatedG.edges():
                            updatedG.remove_edge(u_neighbor, tempVertice)
                            
                if (len(updatedG.edges()) == 0):
                    break
                       
#             for localEdge in list(allEdges):
#                 updatedG = nx.Graph()
#                 updatedG.add_edge(localEdge[0], localEdge[1])
#             neighbors = list(graph.adjacency_list()[tempVertice])
#             neighborsSorted = sorted(neighbors, key=self.sortFn)
#             
#             for neighbor in neighborsSorted:
#                 if allEdges:
#                     C.add(neighbor)
#                     if (len(graph.adjacency_list()[neighbor]) != 0):
#                         for v_neighbor in graph.adjacency_list()[neighbor]:
#                             if (neighbor, v_neighbor) in allEdges:
#                                 allEdges.remove((neighbor, v_neighbor))
#                             if (v_neighbor, neighbor) in allEdges:
#                                 allEdges.remove((v_neighbor, neighbor))


#         while allEdges:
#             temp_edge = allEdges.pop() #heapq.heappop(allEdges)
#             u = temp_edge[0]
#             v = temp_edge[1]
#             C.add(u)
#             C.add(v)
#             C.union(u, v)

         


        return C
    
    def approx_vertex_cover(self, graph):
        C = set()
        
        allEdges = set() #[]
        for edge in graph.edges():
            if not edge in allEdges:
                allEdges.add(edge) #heapq.heappush(allEdges, edge)
        
#         heapq.heapify(allEdges)
        while allEdges:
            temp_edge = allEdges.pop() #heapq.heappop(allEdges)
            u = temp_edge[0]
            v = temp_edge[1]
            C.add(u)
            C.add(v)
#             C.union(u, v)
            if (len(graph.adjacency_list()[u]) != 0):
                for u_neighbor in graph.adjacency_list()[u]:
                    if (u, u_neighbor) in allEdges:
                        allEdges.remove((u, u_neighbor))
                    if (u_neighbor, u) in allEdges:
                        allEdges.remove((u_neighbor, u))
            
            if (len(graph.adjacency_list()[v]) != 0):
                for v_neighbor in graph.adjacency_list()[v]:
                    if (v, v_neighbor) in allEdges:
                        allEdges.remove((v, v_neighbor))
                    if (v_neighbor, v) in allEdges:
                        allEdges.remove((v_neighbor, v))

        return C
    
    def main(self):

#         num_args = len(sys.argv)
# 
#         if num_args < 4:
#             print "error: not enough input arguments"
#             exit(1)
        
        graph_file = "C:\\Users\\schoi332\\Documents\\COURSE\\cse6140\\project\\Data\\jazz.graph" #sys.argv[1] #
        output_file = "C:\\Users\\schoi332\\Documents\\COURSE\\cse6140\\project\\Solutions\\jazz.txt" #sys.argv[3] #

        #Construct graph
        G = self.read_graph(graph_file)
        
        if (len(G.nodes()) > 3000):    
            start_approx = time.clock() #time in seconds
            VC = self.approx_vertex_cover(G) 
            total_time = (time.clock() - start_approx) #to convert to milliseconds
        else:
            start_approx = time.clock()
            VC = self.approx_vertex_cover_priority(G)
            total_time = (time.clock() - start_approx)
        
        #Write initial MST weight and time to file
        output = open(output_file, 'w')
        output.write(str(len(VC)) + " " + str(total_time) + "\n")
        
        for i in range(0, len(VC)):
            if i != 0:
                output.write(", ")
            output.write(str(list(VC)[i]+1))

       
        
if __name__ == '__main__':
    # run the experiments
    runexp = approximation()
    runexp.main()

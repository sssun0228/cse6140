\documentclass[a4paper]{article}

\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage[]{algorithm2e}
\usepackage{enumerate,url}
\newcommand\tab[1][1cm]{\hspace*{#1}}

\title{CSE 6140 Assignment 1\\due Sept. 18, 2017 at 6pm on T-Square }
\date{}

\begin{document}
\maketitle
\begin{center}
\vspace{2 in}
Sun Choi, schoi332\\
\vspace{0.5 in}
Collaborator: Hyungu Choi\\
Sources: Course Slides and J. Kleinberg \\and E. Tardos, \textit{Algorithm Design}, Addison Wesley, 1st ed., 2005.
\vspace{5 in}
\end{center}




\section{Algorithm design and complexity}

The problem consists of finding the lowest floor of a building from
which a box would break when dropping it. The building has $n$~floors,
numbered from $1$ to~$n$, and we have $k$~boxes. There is only one way
to know whether dropping a box from a given floor will break it or
not. Go to that floor and throw a box
from the window of the building. If the box does not break, it can be
collected at the bottom of the building and reused.

The goal is to design an algorithm that returns the index of the lowest floor from
which dropping a box will break it. The algorithm returns $n+1$ if a box does
not break when thrown from the $n$-th floor. The cost of the algorithm, to be kept minimal, is expressed
as the number of boxes that are thrown (note that re-use is allowed).

\begin{enumerate}
\item For $k \geq \lceil \log(n) \rceil$, design an algorithm with
${O}(\log(n))$ boxes thrown.\\

\textbf{Answer}\\
n: the number of floors\\
k: the number of boxes

\begin{itemize}
	\item Step 1. All of the floors are indexed from 1 to $n$
	\item Step 2. Drop a box from $\lfloor \cfrac{1+n}{2} \rfloor^{th}$ floor
	\item Step 3. If it is broken, take the first half of the floors $[1, ..., \lfloor \cfrac{1+n}{2} \rfloor]$\\
	Else, take the rest half $[\lfloor \cfrac{1+n}{2} \rfloor +1, ..., n]$
	\item Step 4. Repeat from Step 1
\end{itemize}

For general expression,
\begin{itemize}
	\item Step 1. Floors, $F = [i, ..., j]$
	\item Step 2. Test $F[\lfloor \cfrac{i+j}{2} \rfloor]$
	\item Step 3. If $F[\lfloor \cfrac{i+j}{2} \rfloor]$ breaks, take $[i, ...,\lfloor \cfrac{i+j}{2} \rfloor]$ and go to step 2 \\
	Else, take $[\lfloor \cfrac{i+j}{2} \rfloor + 1, ..., j]$ and go to step 2
\end{itemize}
Because the algorithm keeps bisecting the floors array, its time complexity is $O(\log n)$.

\item For $k < \lceil \log(n) \rceil$, design an algorithm with
${O}\left(k+\frac{n}{2^{k-1}}\right)$ boxes thrown.\\
\textbf{Answer}\\
\begin{itemize}
	\item Step 1. Use the algorithm of problem 1.1 with $(k-1)$ boxes \\
	At the end of step 1, there will be an array of length $\cfrac{n}{2^{k-1}}$, let's call it L. If we assume a box must be broken if it is thrown at the top of the building. And it must not be broken if it is thrown at the first floor of the building. Then $L = [not \ broken, ...,broken]$
	\item Step 2. Use the last box and test $L$ from the lowest floor
\end{itemize}
Step 1 takes $O(k-1)$ and Step 2 takes $O(\cfrac{n}{2^{k-1}})$. Thus, the the number of thrown boxes is $O(k-1 + \cfrac{n}{2^{k-1}}) = O(k + \cfrac{n}{2^{k-1}})$.

\item For $k=2$, design an algorithm with ${O}(\sqrt{n})$
  boxes thrown.\\
\textbf{Answer}\\
Assume that $n = x^2$ for some integer $x>0$.
\begin{itemize}
	\item Step 1. Divide the floors into $\sqrt{n}$ and each group will have $\sqrt{n}$ floors
	\item Step 2. Starting from the group with lower floors, test the lowest floors of each group
	\item Step 3. If a box is broken at the first floor of $i^{th}$ group, go to $(i-1)^{th}$ group and test each floor of the group from the lowest
	\item Step 4. If a box is not broken for any first floors of groups, test the last group
\end{itemize}
Step 2 takes $O(\sqrt{n})$ and Step 3 takes $O(\sqrt{n})$. Therefore, the number of boxes thrown is $O(2\times \sqrt{n}) = O(\sqrt{n})$
\end{enumerate}



\clearpage
\section{Greedy 1}
A 30 foot long water pipe in Bob's garden has sprung leaks at multiple spots along its seam. As useful as it might be as an irrigation tool, Bob wants to plug the leaks with thin strips of sealant. Each such strip is 9 inches in length and shockingly expensive, so laying down strip after strip in succession along the seam is not a smart idea; and Bob is most definitely a smart man.\\

How can Bob employ the most efficient greedy algorithm, such that all $n$ leaks are plugged with the minimum number of strips? Assume each leak to be tiny in comparison to the sealing strip length. Also state the time complexity of the algorithm and prove its correctness using the concepts of Greedy Choice and Optimal Substructure Properties.\\
\textbf{Answer}\\

\begin{enumerate}
\item Greedy algorithm \\
Label location of $n$ leaks in order, $l_1, ..., l_n$
From $i=1$
\begin{itemize}
	\item Step 1. Put a sealing strip where it can cover $i^{th}$ leak and be as close to the end of pipe as possible like in Figure \ref{pipe}\\
	\begin{figure}[!h]
		\includegraphics[width=\textwidth]{fig/pipe.pdf}
		\label{pipe}
		\caption{Way to cover leaks}
	\end{figure}

	Then any leaks between $l_i$ and $l_i + 9 \text{(inch)}$ are covered. Let's say $i^{th}, ..., j^{th}$ leaks are covered.
	\item Set $i=j+1$ and go back to step 1
\end{itemize}

\item Time complexity \\
$O(n)$

\item Proof: Correctness using the concepts of greedy choice \\
Let's call the greedy solution $A=[a_1, ..., a_k]$ and the optimal solution $O=[o_1, ..., o_m]$. $a_i$ and $o_i$ are the head location of each strip used. And $l_1, ..., l_n$ are location of leaks.\\
A strip must cover at least one leak $\Rightarrow o_1 \leq l_1 \leq o_1 + 9\text{(inch)}$ \\
Greedy puts a strip as close to the end of pipe as possible $\Rightarrow a_1 = l_1$ \\
$\therefore o_1 \leq a_1 $\\
Define $O' = O - o_1 + a_1 = \{a_1, o_2, ..., o_m\}$\\
Because $o_1 \leq a_1$, the first greedy strip covers all of leaks that the first optimal strip covers.\\
$\therefore O'$ is also optimal solution.

\item Optimal substructure property \\
After placing the first strip at $o_1$, we need to cover leaks located after $o_1 + 9 \text{(inch)}$.\\
(Optimal $\#$ of strips for $30 \ \text{foot})$\\
 $= 1 +$ (Optimal $\#$ of strips for $30 \ \text{foot} -(a_1 + 9\ \text{inch})) $\\
$\therefore$ The global optimum $O$ contains $O'$.
\end{enumerate}

\section{Greedy 2}
You are on a hike along the Appalachian Trail when you happen upon an abandoned
mine. Against your better judgement, you venture inside and find a large deposit
of various precious minerals. Luckily, you have a bag and a pickaxe with you,
but you can't carry everything. Therefore, you must figure out how much
of each mineral to take to maximize the value of your bag.

Formally: Given a bag with weight limit $L$, and a list of $n$ minerals, where mineral $i$ has value $v_i$ and
weight $w_i$, design a greedy algorithm to maximize the value of
items you pack in your bag. (Note: $v_i$ is the value of the entierty of item $i$, so if
you decide you only want to take half of item $i$ you only get value $\frac{v_i}{2}$).
Prove your algorithm gives the optimal result
using an \textbf{exchange argument}.

\textbf{Answer}\\
\begin{enumerate}
\item Greedy algorithm
\begin{itemize}
	\item Sort minerals by $\cfrac{v_i}{w_i}$
	\item Set $weight = 0$
	\item From $i = 1$ to $n$: \\
\tab If $(weight + w_i) < L$:\\
\tab \tab Pack $i^{th}$ mineral
\end{itemize}

\item Proof\\
Sorted minerals by $\cfrac{v_i}{w_i}$ $S = \{s_1, ..., s_n\}$\\
Packed minerals $P^* = \{p_1, ..., p_k\}$\\
Suppose $s_1$ does not belong to $P^*$ \\
If ($L$ - (weight of $P^*$)) $\textgreater$ (weight of $s_1$):\\
\tab Should put $s_1$ to $P^*$\\
\tab $\Rightarrow$ Contradiction! $s_1$ should have been in $P^*$\\
Else: Remove any mineral $p_i$ from $P^*$ and put $s_1$ or fraction of $s_1$\\
\tab $P' = P^* - p_i + s_1$ \\
\tab $\cfrac{v_i}{w_i} \textless \cfrac{v(s_1)}{w(s_1)}$\\
\tab $\Rightarrow$ value$(P') \textless$ value$(P^*)$\\
\tab $\Rightarrow$ Contradiction! $s_1$ should have been in $P^*$.

\end{enumerate}


\section{Programming Assignment}

You are to implement {\it either} Prim's or Kruskal's algorithm for finding a Minimum Spanning Tree (MST) of an undirected graph, and evaluate its running time performance on a set of graph instances. The 13 input graphs are RMAT graphs \cite{rmat}, which are synthetic graphs with power-law degree distributions and small-world characteristics.

\subsection{Report}\label{subsec:report}
Write a brief report wherein you:
\begin{itemize}
\item[a)] List which data structures you have used for your choice of algorithm (Prim's/Kruskal's). Explain the reasoning behind your choice and how that has influenced the running time of your algorithms, and the theoretical complexity (i.e., specify the big-Oh for your implementation of each of the two algorithms - computeMST and recomputeMST).\\
\textbf{Answer}\\
Kruskal's algorithm is implemented and data structures I have used are as follows.
\begin{itemize}
	\item Networkx MultiGraph: used to parse all edges in graph file. By using MultiGraph, there is no need to iterate every nodes to add edges and nodes.
	\item Networkx Graph: used to save found the minimum spanning tree from computeMST and recomputeMST. Easy to keep the old MST.
	\item Networkx UnionFind: used in computeMST to save connected component and merge two sets. The union of two sets can be easily found.
\end{itemize}

computeMST consists of sorting, making a set for each node and union-find. Let's say that $m$ and $n$ are the number of edges and the number of nodes in the input file. Then, the theoretical complexity of each components are as follows.
\begin{itemize}
	\item Sorting $O(m\log n)$
	\item Making a set for each node $O(n)$
	\item Union-find $O(m \alpha(m,n))$
\end{itemize}
Therefore the total complexity of computeMST is $O(m\log n)$\\
\\
The inputs to recomputeMST are a new edge data $(u, v, weight)$ and the old MST data. If an edge $(u, v)$ is in the old MST, compare the old weight and weight and decide whether to include it or not. The weight of a new edge is equal to or greater than the old weight, the old MST is kept. Else if the new weight is less than the old weight, replace the old $(u, v)$ with the new one. These steps take $O(1)$. If the edge $u, v$ is not in the old MST, the new edge is added the list of the old MST edges and recompute MST. Recomputing MST is very similar with computeMST but the input is list of edges from the old MST. Thus, the complexity of recomputeMST is $O(n\log n)$.\\

\item[b)] Plots: \\
\textbf{Answer}\\


\begin{figure}[!h]
	\includegraphics[width=\textwidth]{fig/static.pdf}
	\label{static}
	\caption{The number of edges vs. the time for the static MST calculation}
\end{figure}

The time of the static MST scales up linearly with the number of edges the graph has. Compared to the theoretical complexity $O(m\log n)$, the algorithm is better.

\begin{figure}[!h]
	\includegraphics[width=\textwidth]{fig/dynamic.pdf}
	\label{dynamic}
	\caption{The number of edges vs. the time for recomputeMST calculation}
\end{figure}

The time of recomputeMST scales up sublinearly with the number of edges in the graph. In contrast, it grows linearly with the number of nodes in the graph. It is consistent with the theoretical complexity $O(n\log n)$.

\begin{figure}[!h]
	\includegraphics[width=\textwidth]{fig/dynamic2.pdf}
	\label{dynamic2}
	\caption{The number of nodes vs. the time for recomputeMST calculation}
\end{figure}

\end{itemize}


\end{document}

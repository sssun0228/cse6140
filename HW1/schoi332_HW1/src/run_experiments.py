#!/usr/bin/python
##  CSE6140 HW1
##  This assignment requires installation of networkx package if you want to make use of available graph data structures or you can write your own!!
##  Please feel free to modify this code or write your own
import networkx as nx
from networkx.utils import UnionFind
import time
import sys

class RunExperiments:
    def read_graph(self, filename):
        #make sure to add to filename the directory where it is located and the extension .txt
        
        G = nx.MultiGraph()
        with open(filename, 'r') as file:
        
        #if you want to use networkx
            firstLine = False
            
            for line in file:
                if (firstLine == False):
                    firstLine = True
                    continue

                node_and_edge = line.split()
                #if node_and_edge[0] not in G.node:
                #G.add_node(int(node_and_edge[0]))
                #if node_and_edge[1] not in G.node:
                #G.add_node(int(node_and_edge[1]))
                G.add_edge(int(node_and_edge[0]), int(node_and_edge[1]), weight=int(node_and_edge[2]))

        #Write code to add nodes and edges
        #Check out add_node, add_edge in networkx
        return G
    
    def computeMST(self, graph):
        subtrees = UnionFind()
        
        sortedEdges = sorted(graph.edges(data=True), key=lambda edge:edge[2]['weight'])
        T = []
        weight = 0
        
        for node in graph.nodes():
            subtrees[node]
        
        for edge in sortedEdges:
            u = edge[0]
            v = edge[1]
            cost_e = edge[2]['weight']
            
            if subtrees[u] != subtrees[v]:
                T.append(edge)
                subtrees.union(u, v)
                weight += cost_e
                
        return weight, T
    
    def recomputeMST(self, u, v, weight, MSTgraph, MSTweight):
        old_edge = MSTgraph.has_edge(u, v) #check existing edge in MST connecting u and v 
        if old_edge and weight >= MSTgraph.edge[u][v]['weight']: #there is edges connecting u and v in MST
            return MSTweight, MSTgraph
        
        elif old_edge and weight < MSTgraph.edge[u][v]['weight']:
            MSTweight = MSTweight - MSTgraph.edge[u][v]['weight'] + weight
            MSTgraph.remove_edge(u,v)
            MSTgraph.add_edge(u,v,{'weight':weight})
            return MSTweight, MSTgraph
        
        elif not old_edge: # if e=(u,v) is not in MST
            MSTgraph.add_edge(u,v,{'weight':weight})
            sortedEdges = sorted(MSTgraph.edges(data=True), key=lambda edge:edge[2]['weight'])
			
            subtrees = UnionFind() 
            T = []
            MSTweight = 0
            for node in MSTgraph.nodes():
                subtrees[node]
            for edge in sortedEdges:
                u = edge[0]
                v = edge[1]
                cost_e = edge[2]['weight']
                
                if subtrees[u] != subtrees[v]:
                    T.append(edge)
                    subtrees.union(u,v)
                    MSTweight += cost_e
            
            MSTgraph = nx.Graph()
            MSTgraph.add_edges_from(T)

            ##compare weight with the highest weight in cycle and decide whether append or not
            return MSTweight, MSTgraph 
            
    def main(self):

        num_args = len(sys.argv)

        if num_args < 4:
            print "error: not enough input arguments"
            exit(1)
        
        graph_file = sys.argv[1] #"C:\\Users\\schoi332\\Documents\\COURSE\\cse6140\\HW1\\MST\\data\\rmat0406.gr" #
        change_file = sys.argv[2] #"C:\\Users\\schoi332\\Documents\\COURSE\\cse6140\\HW1\\MST\\data\\rmat0406.extra" #
        output_file = sys.argv[3] #"C:\\Users\\schoi332\\Documents\\COURSE\\cse6140\\HW1\\MST\\results\\rmat0406_output.txt" #

        #Construct graph
        G = self.read_graph(graph_file)

        start_MST = time.clock() #time in seconds
        MSTweight, MST = self.computeMST(G) #call MST function to return total weight of MST
        total_time = (time.clock() - start_MST) * 1000 #to convert to milliseconds

        #Write initial MST weight and time to file
        output = open(output_file, 'w')
        output.write(str(MSTweight) + " " + str(total_time) + "\n")

        MSTgraph = nx.Graph()
        MSTgraph.add_edges_from(MST)
        new_weight = MSTweight

        #Changes file
        with open(change_file, 'r') as changes:
            num_changes = changes.readline() #read first line

            for line in changes:
                #parse edge and weight
                edge_data = list(map(lambda x: int(x), line.split()))
                assert(len(edge_data) == 3)

                u,v,weight = edge_data[0], edge_data[1], edge_data[2]                
                
                #call recomputeMST function
                start_recompute = time.clock()
                new_weight, MSTgraph = self.recomputeMST(u, v, weight, MSTgraph, new_weight)#G)
                total_recompute = (time.clock() - start_recompute) * 1000 # to convert to milliseconds
                
                #write new weight and time to output file
                output.write(str(new_weight) + " " + str(total_recompute) + "\n")

if __name__ == '__main__':
    # run the experiments
    runexp = RunExperiments()
    runexp.main()

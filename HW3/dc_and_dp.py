'''
Created on Oct 13, 2017

@author: schoi332
'''
import time
import math
class RunExperiments:
    def dc(self, interest_array, start, end):
        if start == end:
            return (interest_array[start], start, start)
        
        n = float(end - start)
        
        (sum_left, start_left, end_left) = self.dc(interest_array, start, start + int(math.floor(n/2)))
        (sum_right, start_right, end_right) = self.dc(interest_array, start + int(math.floor(n/2)) + 1, end)
        
        left = float(interest_array[start + int(math.floor(n/2))])
        right = float(interest_array[start + int(math.floor(n/2)) + 1])
        total_sum = float(interest_array[start + int(math.floor(n/2))])

        i = start + int(math.floor(n/2))
        j = start + int(math.floor(n/2)) + 1
        
        for p in range(start + int(math.floor(n/2))-1, start-1, -1):
            total_sum += float(interest_array[p])
            if total_sum > left:
                left = total_sum
                i = p
        
        total_sum = float(interest_array[start + int(math.floor(n/2)) + 1])
        
        for q in range(start + int(math.floor(n/2)) + 2, end+1):
            total_sum += float(interest_array[q])
            if total_sum > right:
                right = total_sum
                j = q
        
        if float(sum_left) > max(float(sum_right), float(left) + float(right)):
            return (float(sum_left), int(start_left), int(end_left))
        elif float(sum_right) > max(float(sum_left), float(left) + float(right)):
            return (float(sum_right), int(start_right), int(end_right))
        else:
            return (float(left) + float(right), i, j)    

    def dp(self, interests):
        B = [[0 for x in range(3)] for p in range(self.col)]
        max_sum = float('-inf')
        local_sum = 0
        i = 0
        j = 0
        i_tmp = 0
        
        for q in range(self.col):
            local_sum = local_sum + float(interests[q])
            
            if local_sum > max_sum:
                max_sum = local_sum
                i = i_tmp
                j = q
                
            if local_sum < 0:
                i_tmp = q + 1
                local_sum = 0
                         
            B[q][0] = local_sum
            B[q][1] = i + 1
            B[q][2] = j + 1
        
        return max(B, key=lambda x:x[0])
            
            
    def main(self):
        
        #need to implement iteration through input files
        
        with open('./data/10000.txt', 'r') as file:
            firstline = False
           
            dc_out = open('./output/schoi332_output_dc_10000.txt', 'w')
            dp_out = open('./output/schoi332_output_dp_10000.txt', 'w')
                    
            for line in file:
                if (firstline == False):
                    firstline = True
                    self.col = int(line.split(',')[0])
                    self.row = int(line.split(',')[1])
                    continue
                
                start_dc = time.clock()
                maxsum, i, j = self.dc(line.split(','), 0, self.col-1)
                time_dc = (time.clock() - start_dc)*1000 #to convert to milliseconds
                dc_out.write(str(maxsum) + "," + str(i+1) + "," +str(j+1) + "," +str(time_dc) + "\n" )
                        
                start_dp = time.clock()
                B = self.dp(line.split(',')) 
                time_dp = (time.clock() - start_dp)*1000
                dp_out.write(str(B[0]) + "," + str(B[1]) + "," + str(B[2]) + "," + str(time_dp) + "\n")
                
if __name__ == '__main__':
    # run the experiments
    runexp = RunExperiments()
    runexp.main()

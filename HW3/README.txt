Structures
 ----MST
  |--src: run_experiments.py
  |--results: *_output.txt
  |--data: *.gr, *.extra
  |--runTests.sh

How to run
 Run runTests.sh. It calls ./src/run_experiments.py. 

Tested environments
 Windows 7 Enterprise, python2.7